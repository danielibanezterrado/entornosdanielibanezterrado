
public class ejercicio08 {
	public static void main(String[] args) {
		int num1 = 5;
		int num2 = 3;
		int num3 = -7;

		boolean resultado = (num1 > num3);
		System.out.println(resultado);
		resultado = (num3 > num2);
		System.out.println(resultado);
		
		resultado = (num1 > num3) && (num3 > num2);
		System.out.println(resultado);
		resultado = (num1 > num3) || (num3 > num2);
		System.out.println(resultado);
		resultado = true;
		resultado = !resultado;
		System.out.println(resultado);
	}
}
